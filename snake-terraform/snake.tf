variable "inp" {
    type = string
    default = ""
}

variable "size" {
    type = number
    default = 10
}

resource "random_integer" "apple" {
    min = 1
    max = var.size - 1
    count = 2
}

locals {
    initial_model = {
        size = var.size
        snake = [[4, 5], [4, 6], [4, 7], [4, 8]]
        apple = [random_integer.apple[0].result, random_integer.apple[1].result]
        dir = "u"
        dead = false
    }
}

module "update" {
    source = "./modules/update"
    model = local.initial_model
    inp = var.inp
}

module "view" {
    source = "./modules/view"
    model = module.update.value
}

output "text" {
    value = module.view.text
}
